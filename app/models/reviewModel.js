//khai báo thư viện mongoose
const mongoose = require('mongoose');

//khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//tạo 1 đối tượng schema bao gồm các thuộc tính của collection trong mongoose
const reviewSchama = new Schema({
    _id: mongoose.Types.ObjectId,
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    },
    create_At :{
        type : Date,
        default : Date.now()
    },
    updated_At :{
        type : Date,
        default : Date.now()
    }

});

//export schema ra model
module.exports = mongoose.model("review", reviewSchama)