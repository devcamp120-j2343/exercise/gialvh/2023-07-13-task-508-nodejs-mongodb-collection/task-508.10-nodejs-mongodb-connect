//b1 : khai báo thư viện mongoose
const mongoose = require('mongoose');

//b2 : khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//b3 : tạo dôdsi tượng schema bao gồm các thuộc tính của collection trong mongodb
const courseChema = new Schema({
    _id : mongoose.Types.ObjectId,
    title :{
        type : String,
        required : true,
        uinque : true
    },
    description : {
        type : String,
        required : false
    },
    noStudent : {
        type : Number,
        default : 0
    },
    reviews : [
        {
            type : mongoose.Types.ObjectId,
            ref :"review"
        }
    ]
});

//b4 : export schama ra model
module.exports = mongoose.model("course", courseChema);