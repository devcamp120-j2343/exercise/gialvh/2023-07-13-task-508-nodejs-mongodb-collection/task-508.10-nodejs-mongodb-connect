//khai báo thư viện express
const express = require("express");
const {courseMiddleware} = require('../middlewares/courseMiddleware')

//tạo ra router
const courseRouter = express.Router();

//khai báo middleware
const {
        getAllCoursesMiddleware,
        getCoursesMiddleware,
        postCoursesMiddleware,
        putCoursesMiddleware,
        deleteCoursesMiddleware   
} = require('../middlewares/courseMiddleware');



//sử dụng middle ware
courseRouter.use((req,res,next) =>{
    console.log("Request URL course: ",req.url);
    next();
});

courseRouter.get('/courses',(req,res) =>{
    console.log("day la method " + req.method);
    res.json({
        message : "Get all course"
    })
})

courseRouter.get('/courses/:courseId', getCoursesMiddleware,(req,res)=>{
    let courseId = req.params.courseId;
    res.json({
        message : `Get course Id = ${courseId}`
    })
})

courseRouter.post('/courses/', postCoursesMiddleware,(req,res)=>{
    res.json({
        message : `Create a new Course`
    })
})

courseRouter.put('/courses/:courseId', putCoursesMiddleware,(req,res)=>{
    let courseId = req.params.courseId;
    let body = req.body;

    console.log({courseId,...body});
    res.json({
        message : {courseId,...body}
    })
})

courseRouter.delete('/courses/:courseId', deleteCoursesMiddleware,(req,res)=>{
    let courseId = req.params.courseId;
    res.json({
        message : `Delete Course with id : ${courseId}`
    })
})

module.exports = {courseRouter};
