//khai báo thư viện express
const express = require("express");

//khai báo middleware
const {
    getAllReviewsMiddleware,
    getReviewsMiddleware,
    postReviewsMiddleware,
    putReviewsMiddleware,
    deleteReviewsMiddleware  
} = require('../middlewares/reviewMiddleware');

//tạo ra router
const reviewRouter = express.Router();

//sử dụng middleware
reviewRouter.use((req,res,next) =>{
    console.log("Request URL course: ",req.url);
    next();
});

reviewRouter.get('/reviews',getAllReviewsMiddleware,(req,res) =>{
    console.log(req.method);
    res.json({
        message : "Get all Review"
    })
})

reviewRouter.get('/reviews/:reviewId', getReviewsMiddleware,(req,res)=>{
    let reviewId = req.params.reviewId;
    res.json({
        message : `Get Review Id = ${reviewId}`
    })
})

reviewRouter.post('/reviews/', postReviewsMiddleware,(req,res)=>{
    res.json({
        message : `Create a new Review`
    })
})

reviewRouter.put('/reviews/:reviewId', putReviewsMiddleware,(req,res)=>{
    let reviewId = req.params.reviewId;
    res.json({
        message : `Update Review with id : ${reviewId}`
    })
})

reviewRouter.delete('/reviews/:reviewId', deleteReviewsMiddleware,(req,res)=>{
    let reviewId = req.params.reviewId;
    res.json({
        message : `Delete Review with id : ${reviewId}`
    })
})

module.exports = {reviewRouter}