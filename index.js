//khai báo thư viện express
const express = require('express');

//khai báo router
const { courseRouter } = require('./app/routes/courseRouter');
const { reviewRouter } = require('./app/routes/reviewRouter');

//khai báo thư viện mongoose
const mongoose = require('mongoose');

//khai báo các models mongoose
const reviewModel = require('./app/models/reviewModel');
const courseModel = require('./app/models/courseModel');

// //kết nối vào mongoose
mongoose.connect(`mongodb://127.0.0.1:27017/CRUD_Course`)
.then(()=>console.log("Connected to mongoDB"))
.catch(error => handleError(error));


//khai báo app
const app = express();

//khai báo cổng
const port = 8000;


app.use('/',courseRouter);
app.use("/", reviewRouter);



//khởi động app
app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})